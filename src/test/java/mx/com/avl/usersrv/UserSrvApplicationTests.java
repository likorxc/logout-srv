package mx.com.avl.usersrv;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import mx.com.avl.logout.model.LogoutRequest;
import mx.com.security.cipher.CipherString;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class UserSrvApplicationTests {
	
	public static final String KEY = "DEV005da6b5fd0045d4a7da5eab78b094c3";
	public ObjectMapper mapperObj = new ObjectMapper();
	
	@Test
	public void contextLoads() throws NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, JsonProcessingException {
		CipherString cipher = new CipherString(KEY);
		LogoutRequest login = new LogoutRequest();
		login.setCellPhone("9512012115");
		System.out.println("Cifrado: "+cipher.encryptString(mapperObj.writeValueAsString(login)));
	}

}
