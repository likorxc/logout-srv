package mx.com.avl.logout.model;

import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import mx.com.avl.commons.annotation.Phone;

/**
 * Created by Yanelike on 02/08/2019
 * Descripción: Clase Request para un inicio de sesión
 */
@ApiModel(description = "Class representing a logout request to singout to user")
public @Data class LogoutRequest {
	
    @NotBlank(message= "El usuario no puede ser nulo")
    @Phone(message = "El celular ingresado es inválido")
    @ApiModelProperty(notes="CellPhone should have atleast 10 characters",example ="9512012118", required = true)
    private String cellPhone;

}
