package mx.com.avl.logout.model;

import java.io.Serializable;

import lombok.Data;

/**
 * fecha 15/02/2019
 * @author yanelike
 * descripción datos guardados en cache
 */
public @Data class LoginRedis implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String iuu;
	public String jwtoken;
}
