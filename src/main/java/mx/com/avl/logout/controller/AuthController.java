package mx.com.avl.logout.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import mx.com.avl.commons.dto.ResponseDTO;
import mx.com.avl.logout.business.IUserService;
import mx.com.avl.logout.model.LogoutRequest;

/**
 * fecha: 15/02/2019
 * @author yanelike
 * descripción: Exposición del servicio logout
 */
@RestController
@RequestMapping("secure/login/")
public class AuthController {
    
    @Autowired
    IUserService userService;

    
    @ApiOperation(  value = "User logout",
    	    	    notes = "Returns a information of user and token")
    @RequestMapping(value = "signout", 
    				method = RequestMethod.POST, 
    				produces = MediaType.APPLICATION_JSON_VALUE)    
    public ResponseEntity<ResponseDTO> authenticateUser(
    		@Valid @RequestBody LogoutRequest logoutRequest) {
        return userService.logout(logoutRequest);
    }
    
}
