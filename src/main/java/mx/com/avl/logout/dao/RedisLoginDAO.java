package mx.com.avl.logout.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mx.com.avl.commons.utils.RedisUtil;

/**
 * fecha: 15/02/2019
 * @author yanelike
 * Descripción: Clase implementada para guardar en caché informarmación de un inicio de sesión
 */
@Repository
public class RedisLoginDAO implements IRedisLoginDAO{
	
	@Autowired
	private RedisUtil<String> redisUtil;
	
	
	private static final String TAG_LOGIN = "Login_";

	@Override
	public boolean exist(String key) {
		Object existe = redisUtil.getValue(TAG_LOGIN+key);
		return  existe != null;
	}
	
	@Override
	public boolean deleteKey(String key) {
		redisUtil.delete(key);
		return true;
	}

	@Override
	public String getValue(String key) {
		return redisUtil.getValue(TAG_LOGIN+key);
	}
	
}
