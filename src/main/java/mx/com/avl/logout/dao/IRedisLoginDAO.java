package mx.com.avl.logout.dao;

/**
 * fecha: 15/02/2019
 * @author yanelike
 * Descripción: Interfaz para guardar en caché informarmación de un inicio de sesión
 */
public interface IRedisLoginDAO {
	boolean exist(String key);
	boolean deleteKey(String key);
	String getValue(String key);
}
