package mx.com.avl.logout.business;

import org.springframework.http.ResponseEntity;

import mx.com.avl.commons.dto.ResponseDTO;
import mx.com.avl.logout.model.LogoutRequest;

/**
 * fecha 02/08/2019
 * @author yanelike
 * Descripción: Interfaz para cierre de sesión de un usuario
 */
public interface IUserService {

	/**
	 * 
	 * @param request dato que envia el front para el cierre de sesión.
	 * @return ResponseDTO respuesta del cierre de sesión
	 */
	ResponseEntity<ResponseDTO> logout(LogoutRequest request);

}
