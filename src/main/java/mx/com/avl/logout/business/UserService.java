package mx.com.avl.logout.business;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import mx.com.avl.commons.dto.ResponseDTO;
import mx.com.avl.logout.dao.IRedisLoginDAO;
import mx.com.avl.logout.model.LogoutRequest;

/**
 * fecha  02/08/2019
 * @author yanelike 
 * Descripción: Implementación de servicio para un cierre de sesión
 */

@Service
public class UserService implements IUserService{
	
	private static final Logger LOG = Logger.getLogger(UserService.class);
	
	@Autowired
	private IRedisLoginDAO redisDAO;
	
	@Override
	public ResponseEntity<ResponseDTO> logout(LogoutRequest request) {
		
		ResponseDTO response = new ResponseDTO();
		String jwt = redisDAO.getValue(request.getCellPhone());
		if(jwt != null && !jwt.isEmpty()) {
			redisDAO.deleteKey(request.getCellPhone());
		}else {
			LOG.debug("No existe datos en redis");
			response.setCode("001");
			response.setDescription("Sin registros");
		}
			    
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
}
