package mx.com.avl.logout.aspect;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import mx.com.avl.commons.audit.EventProducer;
import mx.com.avl.commons.constant.EventConstants;
import mx.com.avl.commons.dto.ResponseDTO;
import mx.com.avl.logout.model.LogoutRequest;
 
/**
 * fecha	17/02/2019
 * @author yanelike
 * descripción: Aspecto para guardar los eventos en bitacora.
 */
@Aspect
@Component
public class AspectBusiness {	
	
	private static final Logger LOG = Logger.getLogger(AspectBusiness.class);
	private ObjectMapper mapper = new ObjectMapper();
	
	@Autowired
	private EventProducer eventProducer;
	
	@AfterReturning(pointcut="execution(* mx.com.avl.logout.business.UserService.logout(..))", returning="value")
    public void saveEvent(JoinPoint jp, ResponseEntity<ResponseDTO> value) {
		
		LOG.info("Inicia guardado en auditoria");
		try {
			eventProducer.sendEventMessage(mapper.writeValueAsString((LogoutRequest) jp.getArgs()[0]), 
					mapper.writeValueAsString(value.getBody()), true,
					EventConstants.EVENT_LOGOUT);
		} catch (JsonProcessingException e) {
			LOG.debug("Incidencia al guardar en bitacora cierre de sesion");
		}
		
	}
	
	@AfterThrowing(pointcut="execution(* mx.com.avl.logout.business.UserService.logout(..))", throwing="ex")
    public void throwEvent(JoinPoint jp, Exception ex) {
		LOG.info("Inicia guardado en auditoria fallido");
		try {
			eventProducer.sendEventMessage(mapper.writeValueAsString((LogoutRequest) jp.getArgs()[0]),
					ex.getMessage(), false,
					EventConstants.EVENT_LOGOUT);
		} catch (JsonProcessingException e) {
			LOG.debug("Incidencia al guardar en bitacora cierre de sesion fallido");
		}
	}
	
}
	