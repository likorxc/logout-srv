package mx.com.avl.logout;

import org.apache.log4j.BasicConfigurator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@ComponentScan({"mx.com.avl.logout.*"})
@ComponentScan({"mx.com.security.*"})
@ComponentScan({"mx.com.avl.commons.*"})
@EnableMongoRepositories(basePackages = "mx.com.avl.*")
@EnableSwagger2
@EnableAutoConfiguration()
@EnableDiscoveryClient
@EnableCircuitBreaker
@EnableCaching
public class LogOutApplication {
	
	public static void main(String[] args) {
		BasicConfigurator.configure();
		SpringApplication.run(LogOutApplication.class, args);
	}

}
